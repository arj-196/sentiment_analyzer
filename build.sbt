name := "sentiment"

organization := "fr.eto"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "edu.stanford.nlp" % "stanford-corenlp" % "3.4.1",
  "edu.stanford.nlp" % "stanford-corenlp" % "3.4.1" classifier "models",
//  "edu.stanford.nlp" % "stanford-corenlp" % "3.4.1" classifier "models-french",
  "com.google.guava" % "guava" % "19.0",
  "net.sf.py4j" % "py4j" % "0.10.3",
  "com.github.tototoshi" %% "scala-csv" % "1.3.3"  // csv reader

)

enablePlugins(DockerPlugin)

dockerfile in docker := {
  // The assembly task generates a fat JAR file
  val artifact: File = assembly.value
  val artifactTargetPath = s"/tmp/${artifact.name}"

  new Dockerfile {
    from("java")
    add(artifact, artifactTargetPath)
    entryPoint("java", "-jar", artifactTargetPath)
  }
}

