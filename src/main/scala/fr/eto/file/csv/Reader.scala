package fr.eto.file.csv

import java.io.File
import com.github.tototoshi.csv._

class Reader(filename: String) {

  def readLines(): List[List[String]] = CSVReader.open(new File(filename)).all()

}
