package fr.eto.file.csv

import java.io.File
import com.github.tototoshi.csv.CSVWriter

class Writer(filename: String) {
  val writer = CSVWriter.open(new File(filename))

  def write(content: Seq[Seq[String]]): Unit = {
    writer.writeAll(content)
    writer.flush()
  }

  def close(): Unit = writer.close()
}
