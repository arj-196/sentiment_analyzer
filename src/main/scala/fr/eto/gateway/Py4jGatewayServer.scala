package fr.eto.gateway

import py4j.GatewayServer

/**
  * Created by arj on 21/09/16.
  */
class Py4jGatewayServer {
  private var gatewayServer: GatewayServer = null

  def startGatewayServer(entryPoint: EntryPoint, port: Int): Unit = {
    println("----------------")
    println("Initializing gateway server")
    println("----------------")
    println("Got EntryPoint with cached: " + entryPoint.getCacheReference)
    println("----------------")
    gatewayServer = new GatewayServer(entryPoint, port)
    gatewayServer.start()
  }

}
