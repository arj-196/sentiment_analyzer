package fr.eto.gateway

/**
  * Created by arj on 21/09/16.
  */
class EntryPoint(cached: AnyRef, var nbReq: Int) {
  def this(cached: AnyRef) {
    this(cached, 0)
  }

  def getCacheReference: String= {
    cached.toString
  }

  def getCache(clientId: String): AnyRef = {
    println("responding to client: " + clientId)

    nbReq += 1
    if (nbReq % 10000 == 0) {
      System.gc()
      nbReq = 0
    }

    cached  // return cached object
  }
}
