package fr.eto.gateway.analyzer

import fr.eto.gateway.{Py4jGatewayServer, EntryPoint}
import fr.eto.analyzer.sentiment.SentimentAnalyzer

/**
  * Created by arj on 21/09/16.
  */
class SentimentAnalyzerGatewayServer extends Py4jGatewayServer {
  def listen(): Unit = {
    this.startGatewayServer(
      new EntryPoint(new SentimentAnalyzer),
      25333 // port
    )
  }
}

/**
  * Created by arj on 21/09/16.
  */
object SentimentAnalyzerGatewayServer {
  def main(args: Array[String]) {
    println("-------------")
    println("Run Sentiment Analyzer Gateway Server")
    println("-------------")
     new SentimentAnalyzerGatewayServer listen
  }
}
