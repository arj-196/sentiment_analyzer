package fr.eto.analyzer

/**
  * Created by arj on 21/09/16.
  */
abstract class Analyzer {

  def analyze(input: AnyRef): AnyRef

}
