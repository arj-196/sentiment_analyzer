package fr.eto.analyzer

import fr.eto.analyzer.sentiment.SentimentAnalyzer

class AnalyzerFactory {

  def getAnalyzer(analyzerType: String): Analyzer = {
    analyzerType match {
      case "sentiment" => new SentimentAnalyzer
      case _ => throw new Exception("AnalyzerType: " + analyzerType + " not recognized")
    }
  }
}

object AnalyzerTypes {

  def SentimentAnalyzer: String = "sentiment"

}