package fr.eto.analyzer.sentiment

/**
  * Created by arj on 21/09/16.
  */
class TextWithSentiment(val line: String, val sentiment: Int, val formattedSentiment: String) {
  def getLine: String = line
  def getSentiment: Int = sentiment
  def getFormattedSentiment: String = formattedSentiment
}
