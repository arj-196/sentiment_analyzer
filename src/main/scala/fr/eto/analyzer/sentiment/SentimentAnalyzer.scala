package fr.eto.analyzer.sentiment

import java.util.Properties

import edu.stanford.nlp.ling.CoreAnnotations
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations
import edu.stanford.nlp.pipeline.StanfordCoreNLP
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations
import fr.eto.analyzer.Analyzer
import scala.collection.mutable

class SentimentAnalyzer(strategy: String) extends Analyzer {
  def this() {
    this(SentimentAnalyzerStrategy.LONGEST)
  }

  // Initialising CoreNLP Pipeline
  val props = new Properties()
  props.setProperty("annotators", "tokenize, ssplit, parse, sentiment")
  val pipeline = new StanfordCoreNLP(props)


  override def analyze(rawInput: AnyRef): AnyRef = {
    val input = rawInput.asInstanceOf[String]
    getSentiment(input)
  }

  private def getSentiment(line: String): TextWithSentiment = {
    val (sentiment, formattedSentiment) = computeMainSentiment(line)
    new TextWithSentiment(
      line,
      sentiment,
      formattedSentiment
    )
  }

  /**
    * Compte sentiment for given text
    *
    * @param line
    * @return
    */
  private def computeMainSentiment(line: String): (Int, String) = {
    var predictedClassList = mutable.ListBuffer[Int]()
    var annotationList = mutable.ListBuffer[String]()
    var metaList = mutable.ListBuffer[Int]()

    if (line != null && line.length() > 0) {
      val annotation = pipeline.process(line)
      val sentences = annotation.get(classOf[CoreAnnotations.SentencesAnnotation])

      for (i <- 0 until sentences.size()) {
        val sentence = sentences.get(i)
        val tree = sentence.get(classOf[SentimentCoreAnnotations.AnnotatedTree])

        predictedClassList.+=(RNNCoreAnnotations.getPredictedClass(tree))
        annotationList.+=(sentence.get(classOf[SentimentCoreAnnotations.ClassName]))
        metaList.+=(sentence.toString.length())

      }
    }
    chooseResultByStrategy(
      predictedClassList,
      annotationList,
      metaList
    )
  }

  /**
    * Applying choosen strategy to select optimal sentiment from the list of results
    *
    * @param predictedClassList list of predicted classes
    * @param annotationList     list of predicted sentiment class names
    * @param metaList           list of sentence lengths
    * @return (Int, String) : (predictedClass, predictedClassName)
    */
  private def chooseResultByStrategy(predictedClassList: mutable.ListBuffer[Int],
                                     annotationList: mutable.ListBuffer[String],
                                     metaList: mutable.ListBuffer[_]): (Int, String) = {

    if (strategy == SentimentAnalyzerStrategy.LONGEST)
      __chooseResultStrategyLongest(
        predictedClassList, annotationList, metaList
      )
    else if (strategy == SentimentAnalyzerStrategy.AVERAGE_OPTIMISTIC)
      __chooseResultStrategyAverageOptimistic(
        predictedClassList, annotationList, metaList
      )
    else if (strategy == SentimentAnalyzerStrategy.AVERAGE_PESSIMISTIC)
      __chooseResultStrategyAveragePessimistic(
        predictedClassList, annotationList, metaList
      )
    else
      null
  }

  /**
    * Choosing predicted class based on the predicted class for the longest sentence in text
    *
    * @param predictedClassList list of predicted classes
    * @param annotationList     list of predicted sentiment class names
    * @param metaList           list of sentence lengths
    * @return (Int, String) : (predictedClass, predictedClassName)
    */
  private def __chooseResultStrategyLongest(predictedClassList: mutable.ListBuffer[Int],
                                            annotationList: mutable.ListBuffer[String],
                                            metaList: mutable.ListBuffer[_]): (Int, String) = {
    var (longestIndex, longestValue) = (-1, -1)

    for (i <- metaList.indices) {
      if (metaList(i).asInstanceOf[Int] > longestValue) {
        longestIndex = i
        longestValue = metaList(i).asInstanceOf[Int]
      }
    }
    (predictedClassList(longestIndex), annotationList(longestIndex))
  }

  /**
    * Choosing predicted class as ceiling average of all found sentiment values
    *
    * @param predictedClassList list of predicted classes
    * @param annotationList     list of predicted sentiment class names
    * @param metaList           list of sentence lengths
    * @return (Int, String) : (predictedClass, predictedClassName)
    */
  private def __chooseResultStrategyAverageOptimistic(predictedClassList: mutable.ListBuffer[Int],
                                                      annotationList: mutable.ListBuffer[String],
                                                      metaList: mutable.ListBuffer[_]): (Int, String) = {
    val averageSentiment = math.ceil(predictedClassList.sum.toFloat / predictedClassList.length).toInt
    (averageSentiment, SentimentClassifierResults.classes(averageSentiment))
  }

  /**
    * Choosing predicted class as floor average of all found sentiment values
    *
    * @param predictedClassList list of predicted classes
    * @param annotationList     list of predicted sentiment class names
    * @param metaList           list of sentence lengths
    * @return (Int, String) : (predictedClass, predictedClassName)
    */
  private def __chooseResultStrategyAveragePessimistic(predictedClassList: mutable.ListBuffer[Int],
                                                       annotationList: mutable.ListBuffer[String],
                                                       metaList: mutable.ListBuffer[_]): (Int, String) = {
    val averageSentiment = math.floor(predictedClassList.sum.toFloat / predictedClassList.length).toInt
    (averageSentiment, SentimentClassifierResults.classes(averageSentiment))
  }

}

object SentimentAnalyzerStrategy {
  val LONGEST = "longest"
  val AVERAGE_OPTIMISTIC = "avg_optimistic"
  val AVERAGE_PESSIMISTIC = "avg_pessimistic"
}


private object SentimentClassifierResults {
  val classes = Map(
    0 -> "very negative",
    1 -> "negative",
    2 -> "neutral",
    3 -> "positive",
    4 -> "very positive"
  )
}